import React, { useState, useEffect } from 'react';

import './GeneralPage.scss'

const GeneralPage = () => {
    return(
        <div className="general_page">
            <div className="general_page__title">Главная страница</div>
        </div>
    );
};

export default GeneralPage;