import React from 'react';
import './PageLoading.scss';

const PageLoading = () => {
    return(
        <div className="page_loading">
            <div className="page_loading__title">Страница Загружается</div>
        </div>
    )
}

export default PageLoading;